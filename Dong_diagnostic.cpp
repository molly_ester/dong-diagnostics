#include <iostream>
#include <string>
#include<time.h>
#include<conio.h>

using namespace std;
void outputNumbers(int* numbers) {
	for (int i = 0; i < 10; i++) cout << numbers[i] << " ";
}
void bubbleSort(int sort, int* numbers, int size) {
	for (int i = 0; i < size; i++) {
		for (int k = 0; k < size - i - 1; k++) {
			if (numbers[k] > numbers[k + 1] && sort == 1) swap(numbers[k], numbers[k + 1]);
			else if (numbers[k] < numbers[k + 1] && sort == 2) swap(numbers[k], numbers[k + 1]);
			
		}
	}
}

void selectionSort(int sort, int* numbers, int size) {
	for (int i = 0; i < size; i++) {
		int placeHolder = numbers[i];
		int element = i;
		
		for (int j = i + 1; j < size; j++) {
			if (placeHolder > numbers[j] && sort == 4) {
				placeHolder = numbers[j];
				element = j;
			}
			else if (placeHolder < numbers[j] && sort == 5) {
				placeHolder = numbers[j];
				element = j;
			}
		}
		swap(numbers[i], numbers[element]);
	}
}

bool linearSearch(int* numbers, int input, int& steps) {
	bool isFound = false;
	for (int i = 0; i < 10; i++) {
		steps++;
		if (input == numbers[i]) {
			isFound = true;
			break;
		}
	}
	return isFound;
}

int main(){
	srand(time(NULL));
	int numbers[10];
	int input;

	for (int i = 0; i < 10; i++) {
		numbers[i] = rand() % 69 + 1;
	}

	while (true) {
		outputNumbers(numbers);
		cout << "\n\n1. ascending bubbleSort\n2. descending bubbleSort\n3. Linear Search\n4. ascending SelectionSort\n5. Descending SelectionSort\nInput: ";
		do cin >> input; while (input > 5 || input < 1);
		
		if (input < 3) {
			bubbleSort(input, numbers, 10);
			outputNumbers(numbers);
		}
		else if (input == 3) {
			int steps = 0;
			cout << "what number to search?\ninput: ";
			cin >> input;
			bool isFound = linearSearch(numbers, input, steps);

			if (isFound) cout <<  "Item Found...";
			else cout << "Item not Found...";
			cout << "it Took " << steps << " steps to complete the process";
		}
		else {
			selectionSort(input, numbers, 10);
			outputNumbers(numbers);
		}

		_getch();
		system("Cls");
	}
	return 0;
}